package ru.tsc.fuksina.tm.command.project;

import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-index";

    public static final String DESCRIPTION = "Complete project by index";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.COMPLETED);
    }
}
