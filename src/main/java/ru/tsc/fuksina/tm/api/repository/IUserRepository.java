package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User removeUser(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

}
