package ru.tsc.fuksina.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
