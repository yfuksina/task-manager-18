package ru.tsc.fuksina.tm.api.model;

import java.util.Date;

public interface IHasDateStart {

    Date getDateStart();

    void setDateStart(Date dateStart);
}
